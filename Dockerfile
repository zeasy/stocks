FROM daocloud.io/zeasy/docker_images_strongloop:master-7a5d9ed

EXPOSE 3000
RUN mkdir -p /
ADD * /
CMD sh ./startup.sh

RUN npm install --save loopback-connector-mysql
RUN npm install --save loopback-connector-mongodb

RUN echo ok!!!
